using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CloseButton : MonoBehaviour
{
    private void OnMouseUpAsButton()
    {
        transform.parent.gameObject.SetActive(false);
    }
}
