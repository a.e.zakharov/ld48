﻿using System;
using UnityEngine;
using UnityEngine.UI;

public class SlideBar : MonoBehaviour
{
    public Slider slider;

    public void SetMaxValue(float value)
    {
        slider.maxValue = value;
        SetValue(value);
    }
    
    public void SetValue(float value)
    {
        slider.value = value;
    }
}
