﻿using UnityEngine;
using UnityEngine.SceneManagement;

namespace Resources.Ui.Scripts
{
    public class FinishButton : MonoBehaviour
    {
        private void OnMouseUpAsButton()
        {
            SceneManager.LoadScene("SampleScene");
        }
    }
}