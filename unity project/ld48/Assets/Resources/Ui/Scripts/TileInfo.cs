using TMPro;
using UnityEngine;

public class TileInfo : MonoBehaviour
{
    public Transform button;

    public TextMeshPro textMesh;

    public BaseTile tile { get; private set; }

    public void SetTile(BaseTile newTile)
    {
        tile = newTile;
        button.gameObject.SetActive(!tile.researched);
        textMesh.text = tile.tileExplorationInfo.TextBeforeExploration;
    }
}
