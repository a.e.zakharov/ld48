using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;
using Random = UnityEngine.Random;

public class TileInfoButton : MonoBehaviour
{
    public UiController uiController;

    public TileInfo tileInfo;

    public HeroStats stats;
    
    private void OnMouseUpAsButton()
    {
        if (!uiController.canOpenTiles) return;
        if (tileInfo.tile.researched)
        {
            gameObject.SetActive(false);
            return;
        }

        var explorationInfo = tileInfo.tile.tileExplorationInfo;
        if (explorationInfo == null) return;
        
        var text = new StringBuilder(explorationInfo.TextAfterExploration);
        if (explorationInfo.StatValues.Length > 0)
        {
            text.Append("\n" + String.Join("\n", explorationInfo.StatValues.Where(se => se.AddValue != 0).Select(se =>
            {
                var addValueStr = se.AddValue > 0 ? $"+{se.AddValue}" : se.AddValue.ToString();
                return $"<b>{se.StatName}</b>: {addValueStr}";
            })));
        }
        
        var statsToAdd = new List<AddStatValue>(explorationInfo.StatValues);
        if (explorationInfo.TileEnemy != null)
        {
            var chance = Random.value * 100;
            if (chance <= explorationInfo.AttackChance)
            {
                statsToAdd.Add(new AddStatValue(HeroStats.Health, explorationInfo.TileEnemy.Damage));
                text.Append($"\n\n{explorationInfo.TileEnemy.AttackText}\n<b>{HeroStats.Health}</b>: {explorationInfo.TileEnemy.Damage}");
            }
        }
        
        statsToAdd.Add(new AddStatValue(HeroStats.Stamina, -1));
        stats.AddStats(statsToAdd.ToArray());
        uiController.ShowText(text.ToString());
        
        tileInfo.tile.SetResearched(true);
        gameObject.SetActive(false);
    }
}
