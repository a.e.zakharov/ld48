using System;
using TMPro;
using UnityEngine;

public class UiController : MonoBehaviour
{
    public Transform textWindow;

    public Transform finishWindow;

    public SlideBar healthBar;

    public SlideBar hungerBar;

    public SlideBar staminaBar;

    public ResourceCounter leavesCounter;

    public ResourceCounter rocksCounter;

    public ResourceCounter sticksCounter;

    public ResourceCounter vinesCounter;

    public ResourceCounter woodCounter;

    public HeroStats heroStats;

    public TileInfo tileInfo;

    public bool canOpenTiles => !textWindow.gameObject.activeSelf && !finishWindow.gameObject.activeSelf;

    private void Awake()
    {
        healthBar.SetMaxValue(heroStats.Stats[HeroStats.Health].MaxValue);
        hungerBar.SetMaxValue(heroStats.Stats[HeroStats.Hunger].MaxValue);
        staminaBar.SetMaxValue(heroStats.Stats[HeroStats.Stamina].MaxValue);
    }

    private void Update()
    {
        healthBar.SetValue(heroStats.Stats[HeroStats.Health].CurrentValue);
        hungerBar.SetValue(heroStats.Stats[HeroStats.Hunger].CurrentValue);
        staminaBar.SetValue(heroStats.Stats[HeroStats.Stamina].CurrentValue);
        
        leavesCounter.SetText($"{heroStats.Stats[HeroStats.Leaves].CurrentValue}/{heroStats.StatsToWin[HeroStats.Leaves]}");
        rocksCounter.SetText(heroStats.Stats[HeroStats.Rocks].CurrentValue.ToString());
        sticksCounter.SetText($"{heroStats.Stats[HeroStats.Sticks].CurrentValue}/{heroStats.StatsToWin[HeroStats.Sticks]}");
        vinesCounter.SetText($"{heroStats.Stats[HeroStats.Vines].CurrentValue}/{heroStats.StatsToWin[HeroStats.Vines]}");
        woodCounter.SetText($"{heroStats.Stats[HeroStats.Wood].CurrentValue}/{heroStats.StatsToWin[HeroStats.Wood]}");
        
        if (textWindow.gameObject.activeSelf) return;
        if (heroStats.Stats[HeroStats.AlternateEnding].CurrentValue > 0) ShowFinishText("<size=+5>Success?</size>\n\nYou feel guilty. What will happen to those, who came here to live, not to die? Will they have strength to do something that you couldn't. You drift away, looking at the boundless sea. Crash! Something hits the bottom of your raft and you are tumbling in the air...");
        if (heroStats.Stats[HeroStats.Health].CurrentValue == 0) ShowFinishText("Nature takes its own. It can destroy anything that comes her way.\nYou died");
        if (heroStats.Stats[HeroStats.Hunger].CurrentValue == 0) ShowFinishText("Strength left you, no more walking around, no more trying to chew on stale branches in desperate attempt to satiate yourself. Sun is setting down, casting its last rays on your body in the shadow of a berry bush.\nYou died");
        if (heroStats.Stats[HeroStats.Stamina].CurrentValue == 0) ShowFinishText("Conscience leaves you. You lost it, thirst is maddening and you faint after taking last desperate steps in the direction of where river used to be. Wind rises, rainfall hits your dead body.\nYou died");
        if (heroStats.WinByStats) ShowFinishText("<size=+5>Success!</size>\n\nYou have everything to build a raft. Hope is overwheling you! You leave the island with everything you've got and drift away, looking at the boundless sea. After you fall asleep wind gently changes its direction..");
    }

    public void ShowText(string text)
    {
        var textMesh = textWindow.GetComponent<TextMeshPro>();
        textMesh.text = text;
        textWindow.gameObject.SetActive(true);
        
        //ScreenCapture.CaptureScreenshot(Application.dataPath + "/screenshots/" + DateTime.Now.ToString("yyyy-MM-dd HH-mm-ss") + ".png");
        //UnityEditor.AssetDatabase.Refresh();
    }

    public void ShowFinishText(string text)
    {
        tileInfo.gameObject.SetActive(false);
        text += "\n\nThanks for playing!";
        
        var textMesh = finishWindow.GetComponent<TextMeshPro>();
        textMesh.text = text;
        finishWindow.gameObject.SetActive(true);
    }

    public void UpdateTileInfo(BaseTile tile)
    {
        tileInfo.SetTile(tile);
    }
}
