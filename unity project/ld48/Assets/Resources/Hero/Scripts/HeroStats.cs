using System.Collections.Generic;
using UnityEngine;

public class HeroStats : MonoBehaviour
{
    public static string Health = "Health";
    public static string Hunger = "Hunger";
    public static string Stamina = "Stamina";
    public static string Sticks = "Sticks";
    public static string Leaves = "Leaves";
    public static string Rocks = "Rocks";
    public static string Wood = "Wood";
    public static string Vines = "Vines";
    public static string AlternateEnding = "AlternateEnding";
    
    public Dictionary<string, HeroStatPosition> Stats { get; }
    
    public Dictionary<string, int> StatsToWin { get; }
    
    public bool WinByStats { get; private set; }

    public HeroStats()
    {
        Stats = new Dictionary<string, HeroStatPosition>();
        Stats.Add(Health, new HeroStatPosition(15, 15));
        Stats.Add(Hunger, new HeroStatPosition(15, 15));
        Stats.Add(Stamina, new HeroStatPosition(15, 15));
        Stats.Add(Sticks, new HeroStatPosition(0, 0));
        Stats.Add(Leaves, new HeroStatPosition(0, 0));
        Stats.Add(Rocks, new HeroStatPosition(0, 0));
        Stats.Add(Wood, new HeroStatPosition(0, 0));
        Stats.Add(Vines, new HeroStatPosition(0, 0));
        Stats.Add(AlternateEnding, new HeroStatPosition(0, 1));

        StatsToWin = new Dictionary<string, int>();
        StatsToWin.Add(Sticks, 5);
        StatsToWin.Add(Leaves, 20);
        StatsToWin.Add(Vines, 20);
        StatsToWin.Add(Wood, 5);
    }

    public void AddStats(AddStatValue[] values)
    {
        foreach (var value in values)
        {
            var delta = Stats[value.StatName].AddValue(value.AddValue);
            if (value.StatName == Hunger && delta > 0) Stats[Health].AddValue(delta);
        }
        
        var counter = 0;
        foreach (var stat in StatsToWin)
        {
            if (Stats[stat.Key].CurrentValue < stat.Value) break;
            
            counter++;
        }

        WinByStats = counter == StatsToWin.Count;
    }
}
