using System;

public class AddStatValue
{
    public string StatName { get; }

    public int AddValue { get; private set; }
    
    public AddStatValue(string statName, int addValue)
    {
        StatName = statName;
        AddValue = addValue;
    }
    
    public AddStatValue(string statName, Func<int> valueFunc)
    {
        StatName = statName;
        AddValue = valueFunc();
    }
}
