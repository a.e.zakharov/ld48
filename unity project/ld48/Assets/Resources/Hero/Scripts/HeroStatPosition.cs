using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HeroStatPosition
{
    public int CurrentValue { get; private set; }
    
    public int MaxValue { get; }
    
    public HeroStatPosition(int currentValue, int maxValue)
    {
        CurrentValue = currentValue;
        MaxValue = maxValue;
    }

    public int AddValue(int addTo)
    {
        var newValue = CurrentValue + addTo;
        if (newValue < 0) CurrentValue = 0;
        else if (newValue > MaxValue && MaxValue > 0) CurrentValue = MaxValue;
        else CurrentValue = newValue;

        if (newValue > CurrentValue) return newValue - CurrentValue;
        return 0;
    }
}
