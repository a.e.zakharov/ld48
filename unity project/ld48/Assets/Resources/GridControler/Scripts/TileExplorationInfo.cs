﻿namespace Resources.GridControler.Scripts
{
    public class TileExplorationInfo
    {
        public static TileExplorationInfo Default = new TileExplorationInfo {TextBeforeExploration = "Nothing here"};

        public static TileExplorationInfo Camp = new TileExplorationInfo {TextBeforeExploration = "Your camp"};
        
        public string TextBeforeExploration { get; set; }
        
        public string TextAfterExploration { get; set; }
        
        public AddStatValue[] StatValues { get; set; }
        
        public Enemy TileEnemy { get; set; }
        
        public int AttackChance { get; set; }
        
        public TileType Type { get; set; }
    }
}