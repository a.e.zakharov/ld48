namespace Resources.GridControler.Scripts
{
    public class Enemy
    {
        public static Enemy SeaBirds = new Enemy {Power = 2, Damage = -1, AttackText = "While you were gathering supplies, flock of birds tried to gauge your eyes out."};

        public static Enemy Medusa = new Enemy {Power = 2, Damage = -1, AttackText = "You didn't see hidden menace in the sea foam. Suddenly you feel a sting. Your lag has nasty rash now."};

        public static Enemy Stingray = new Enemy {Power = 4, Damage = -3, AttackText = "Trying to catch a fish you reach for something completely different. Your heart stops for a moment and you almost drown. It is not easy to forget this kind of shock."};

        public static Enemy Shark = new Enemy {Power = 7, Damage = -6, AttackText = "You are too far away from the shore. Sudden pain makes you aware that your toe is missing."};

        public static Enemy Crocodile = new Enemy {Power = 5, Damage = -4, AttackText = "Water in this place is too muddy and it is hard to see the predator. Fast pounce and you feel its teeth on your leg, you almost get your lungs full of mud, but finally you break free and run for your life."};

		public static Enemy Beast = new Enemy {Power = 3, Damage = -3, AttackText = "Hairy mountain of rage bumps into you. You somehow manage to make it go away, but now you undertand that island is not amused to have you on its back."};

		public static Enemy Deer = new Enemy {Power = 3, Damage = -3, AttackText = "Hunt for the small animal goes awry. You find something much more massive. Giant horns hit you against the tree. When you wake up, there is no one in sight."};

		public static Enemy Bear = new Enemy {Power = 3, Damage = -3, AttackText = "Terrifying roar hits you. In a second, giant claws tear your game apart and you are hit by them too. Beast is not interested in you and you run away."};

		public static Enemy Flock = new Enemy {Power = 5, Damage = -5, AttackText = "They are charging right into you. You feel like your ribs are on fire after they finally hit you."};

		public static Enemy Snakes = new Enemy {Power = 2, Damage = -2, AttackText = "Something is trying to choke you. Before you shake it away the thing bites you back."};

		public static Enemy Bugs = new Enemy {Power = 1, Damage = -1, AttackText = "Bite, another one. They are swarming around, craving for you blood. You try to make a run for it."};
        
        public int Power { get; set; }
        
        public int Damage { get; set; }
        
        public string AttackText { get; set; }
    }
}
