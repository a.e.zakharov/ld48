using System.Linq;
using Resources.GridControler.Scripts;
using UnityEngine;
using UnityEngine.Tilemaps;
using Random = UnityEngine.Random;

public class GridController : MonoBehaviour
{
    public UiController uiController;
    
    public Grid gridData;

    public int eventChanceStart;
    
    private int _eventChance;

    public int eventChanceIncrement;

    public int explorationChance;
    
    public BaseTile currentTile { get; private set; }
    
    public SandTile alternateEndingTile { get; private set; }

    private GridInformation _gridInformation;

    private TileEvent[] _events;

    private TileExplorationInfo[] _explorationInfos;

    private void Awake()
    {
        #region dictionaries

        _events = new[]
        {
            new TileEvent{ 
                Executed = false,
                Name = "Infection", 
                Text = "You feel dizzy and throw up. You find someplace safe and sleep on your disease for 24 hours.",
                Types = new []
                {
                    TileType.Forest, TileType.Jungle, TileType.Mountain, TileType.Sand, TileType.DrinkWater
                }, 
                StatsChange = new []
                {
                    new AddStatValue(HeroStats.Health, -2),
                    new AddStatValue(HeroStats.Stamina, -2),
                    new AddStatValue(HeroStats.Hunger, -1)
                }},
            new TileEvent{ 
                Executed = false,
                Name = "Mosquitoes", 
                Text = "They are everywhere. EVERYWHERE.",
                Types = new []
                {
                    TileType.Jungle, TileType.DrinkWater
                }, 
                StatsChange = new []
                {
                    new AddStatValue(HeroStats.Health, -1),
                }},
            new TileEvent{ 
                Executed = false,
                Name = "Heatwave", 
                Text = "Sun is scorching hot, you are thirsty.",
                Types = new []
                {
                    TileType.Mountain, TileType.Sand
                }, 
                StatsChange = new []
                {
                    new AddStatValue(HeroStats.Stamina, -1),
                    new AddStatValue(HeroStats.Hunger, -1)
                }},
            new TileEvent{ 
                Executed = false,
                Name = "Cut", 
                Text = "Many minor cuts are now joined by a major wound, you must find something to stem the bleeding.",
                Types = new []
                {
                    TileType.Forest, TileType.Jungle, TileType.Sand
                }, 
                StatsChange = new []
                {
                    new AddStatValue(HeroStats.Health, -1),
                    new AddStatValue(HeroStats.Leaves, -2),
                    new AddStatValue(HeroStats.Vines, -1)
                }},
            new TileEvent{ 
                Executed = false,
                Name = "Bone fracture", 
                Text = "One small misstep for a human which means totally nothing for the mankind. Create a splint to mend yourself.",
                Types = new []
                {
                    TileType.Mountain
                }, 
                StatsChange = new []
                {
                    new AddStatValue(HeroStats.Health, -3),
                    new AddStatValue(HeroStats.Stamina, -1),
                    new AddStatValue(HeroStats.Hunger, -1),
                    new AddStatValue(HeroStats.Sticks, -2),
                    new AddStatValue(HeroStats.Vines, -1)
                }},
            new TileEvent{ 
                Executed = false,
                Name = "Rainfall", 
                Text = "Water is coming right into your hands. And your head and everywhere to be honest.",
                Types = new []
                {
                    TileType.Forest, TileType.Jungle, TileType.Mountain, TileType.Sand, TileType.DrinkWater
                }, 
                StatsChange = new []
                {
                    new AddStatValue(HeroStats.Stamina, +3),
                }},
            new TileEvent{ 
                Executed = false,
                Name = "Shadow figure spotted", 
                Text = "You try to get figure's attention, but instead of greeting you, figure starts to run. When you finally caught up with it, you find only an enraged ape. Easy prey for you, easier than human anyway.",
                Types = new []
                {
                    TileType.Forest, TileType.Jungle, TileType.Mountain, TileType.Sand, TileType.DrinkWater
                }, 
                StatsChange = new []
                {
                    new AddStatValue(HeroStats.Hunger, +6),
                    new AddStatValue(HeroStats.Stamina, +1)
                }},
            new TileEvent{ 
                Executed = false,
                Name = "Tracks", 
                Text = "You see somebodies tracks. After following them you discover an abandoned camp, no one else is around, but there are some supplies left.",
                Types = new []
                {
                    TileType.Sand
                }, 
                StatsChange = new []
                {
                    new AddStatValue(HeroStats.Stamina, +4),
                    new AddStatValue(HeroStats.Hunger, +5),
                    new AddStatValue(HeroStats.Vines, +5),
                    new AddStatValue(HeroStats.Rocks, +3),
                    new AddStatValue(HeroStats.Wood, +1)
                }},
            new TileEvent{ 
                Executed = false,
                Name = "Old bonefire", 
                Text = "Running around the camp to find somebody else on this forsaken island only made you tired and sad.",
                Types = new []
                {
                    TileType.Forest, TileType.Mountain
                }, 
                StatsChange = new []
                {
                    new AddStatValue(HeroStats.Stamina, -1)
                }},
            new TileEvent{ 
                Executed = false,
                Name = "Wind rises", 
                Text = "You have a bad feeling about this. But that's just a feeling.",
                Types = new []
                {
                    TileType.Forest, TileType.Jungle, TileType.Mountain, TileType.Sand, TileType.DrinkWater
                }, 
                StatsChange = new AddStatValue[0]
            },
            new TileEvent{ 
                Executed = false,
                Name = "Cold bite", 
                Text = "The night is cold. You must burn some wood to stay alive and well.",
                Types = new []
                {
                    TileType.Mountain, TileType.Sand
                }, 
                StatsChange = new []
                {
                    new AddStatValue(HeroStats.Sticks, -2),
                }},
            new TileEvent{ 
                Executed = false,
                Name = "Storm", 
                Text = "Nature tries to claim what is hers. Luckily it's just some supplies and not your life.",
                Types = new []
                {
                    TileType.Sand
                }, 
                StatsChange = new []
                {
                    new AddStatValue(HeroStats.Hunger, -2),
                    new AddStatValue(HeroStats.Vines, -2),
                    new AddStatValue(HeroStats.Sticks, -2),
                    new AddStatValue(HeroStats.Leaves, -4)
                }},
            new TileEvent{ 
                Executed = false,
                Name = "Fishing galore", 
                Text = "Whoa. Fishes are just jumping out of the water and right into your mouth.",
                Types = new []
                {
                    TileType.DrinkWater
                }, 
                StatsChange = new []
                {
                    new AddStatValue(HeroStats.Hunger, +4)
                }},
            new TileEvent{ 
                Executed = false,
                Name = "Rapids", 
                Text = "Stream catches you and you find yourself bashed against the stones.",
                Types = new []
                {
                    TileType.DrinkWater
                }, 
                StatsChange = new []
                {
                    new AddStatValue(HeroStats.Health, -2),
                }},
            new TileEvent{ 
                Executed = false,
                Name = "Cave", 
                Text = "There is a small entrance in the mountain's slope. The cave feels habitated.. this is the last thought in your head that right now is haveing a meeting with heavy stone.",
                Types = new []
                {
                    TileType.Mountain
                }, 
                StatsChange = new []
                {
                    new AddStatValue(HeroStats.Health, -10),
                }},
            new TileEvent{ 
                Executed = false,
                Name = "Fire!", 
                Text = "Smoke is in the air. When you look closer, you can see forest fire roaring. RUN.",
                Types = new []
                {
                    TileType.Forest
                }, 
                StatsChange = new []
                {
                    new AddStatValue(HeroStats.Stamina, -1),
                }},
        };

        _explorationInfos = new[]
        {
            new TileExplorationInfo
            {
                TextBeforeExploration = "Walking down the shore you can see several birds fighting for something in the sand",
                TextAfterExploration = "You found a small batch of eggs in the sand",
                StatValues = new []
                {
                    new AddStatValue(HeroStats.Hunger, () => Random.Range(3, 5)),
                    new AddStatValue(HeroStats.Stamina,() => Random.Range(1, 2))
                },
                TileEnemy = Enemy.SeaBirds,
                AttackChance = 100,
                Type = TileType.Sand
            },
            new TileExplorationInfo
            {
                TextBeforeExploration = "Walking down the shore you can see several birds fighting for something in the sand",
                TextAfterExploration = "You scare the birds away only to discover that they were guarding garbage",
                StatValues = new AddStatValue[0],
                TileEnemy = null,
                AttackChance = 0,
                Type = TileType.Sand
            },
            new TileExplorationInfo
            {
                TextBeforeExploration = "There is a fish in the water",
                TextAfterExploration = "You try to catch a fish with your bare hands, but all you get is bruises and cuts",
                StatValues = new []
                {
                    new AddStatValue(HeroStats.Health, -1)
                },
                TileEnemy = Enemy.Medusa,
                AttackChance = 50,
                Type = TileType.Sand
            },
            new TileExplorationInfo
            {
                TextBeforeExploration = "There is a fish in the water",
                TextAfterExploration = "You try to catch a fish with your bare hands, but all you get is bruises and cuts",
                StatValues = new []
                {
                    new AddStatValue(HeroStats.Health, -1)
                },
                TileEnemy = Enemy.Stingray,
                AttackChance = 30,
                Type = TileType.Sand
            },
            new TileExplorationInfo
            {
                TextBeforeExploration = "There is a fish in the water",
                TextAfterExploration = "You catch fish with your bare hands. Fabulous!",
                StatValues = new []
                {
                    new AddStatValue(HeroStats.Hunger, ()=> Random.Range(4, 6))
                },
                TileEnemy = Enemy.Shark,
                AttackChance = 20,
                Type = TileType.Sand
            },
            new TileExplorationInfo
            {
                TextBeforeExploration = "Several crabs are roaming the beach",
                TextAfterExploration = "You try to catch one, but they are too nimble. You suddenly feel really tired",
                StatValues = new []
                {
                    new AddStatValue(HeroStats.Stamina, () => -Random.Range(1, 3))
                },
                TileEnemy = null,
                AttackChance = 0,
                Type = TileType.Sand
            },
            new TileExplorationInfo
            {
                TextBeforeExploration = "Several crabs are roaming the beach",
                TextAfterExploration = "With an astounding feat of agility you catch them one by one. Let's feast!",
                StatValues = new []
                {
                    new AddStatValue(HeroStats.Hunger, () => Random.Range(3, 6)),
                },
                TileEnemy = Enemy.SeaBirds,
                AttackChance = 20,
                Type = TileType.Sand
            },
            new TileExplorationInfo
            {
                TextBeforeExploration = "There is a fish in the water",
                TextAfterExploration = "You try to catch a fish with your bare hands, but all you get is bruises and cuts. With sad determination you fill your flask with water and collect rocks that look usefull.",
                StatValues = new []
                {
                    new AddStatValue(HeroStats.Health, () => -1),
                    new AddStatValue(HeroStats.Stamina, () => Random.Range(2, 4)),
                    new AddStatValue(HeroStats.Rocks, () => Random.Range(0, 1))
                },
                TileEnemy = null,
                AttackChance = 0,
                Type = TileType.DrinkWater
            },
            new TileExplorationInfo
            {
                TextBeforeExploration = "There is a fish in the water",
                TextAfterExploration = "Your movements are gracefull and precise, and now you've got a handfull of fish. Take your time and treat yourself with sweet water.",
                StatValues = new []
                {
                    new AddStatValue(HeroStats.Stamina, () => Random.Range(3, 4)),
                    new AddStatValue(HeroStats.Hunger, () => Random.Range(5, 7))
                },
                TileEnemy = null,
                AttackChance = 0,
                Type = TileType.DrinkWater
            },
            new TileExplorationInfo
            {
                TextBeforeExploration = "There is something sparking in the water",
                TextAfterExploration = "Water is so clear that you've miscalculated river's depth. You found nothing, but at least your flask is now full.",
                StatValues = new []
                {
                    new AddStatValue(HeroStats.Stamina, () => Random.Range(3, 5))
                },
                TileEnemy = null,
                AttackChance = 0,
                Type = TileType.DrinkWater
            },
            new TileExplorationInfo
            {
                TextBeforeExploration = "There is something sparking in the water",
                TextAfterExploration = "Underwater current is really strong here, and you are being pulled down by it. After a short struggle you manage to break free but now you are completely exhausted",
                StatValues = new []
                {
                    new AddStatValue(HeroStats.Health, () => -1),
                    new AddStatValue(HeroStats.Stamina, () => -2)
                },
                TileEnemy = null,
                AttackChance = 0,
                Type = TileType.DrinkWater
            },
            new TileExplorationInfo
            {
                TextBeforeExploration = "Small sand isle is cut out of the shore. You see something there.",
                TextAfterExploration = "You manage to cross the river on feet, but all you find on the isle are only someone's gory remains. You hurry back to safety.",
                StatValues = new []
                {
                    new AddStatValue(HeroStats.Stamina, () => Random.Range(1, 3))
                },
                TileEnemy = Enemy.Crocodile,
                AttackChance = 30,
                Type = TileType.DrinkWater
            },
            new TileExplorationInfo
            {
                TextBeforeExploration = "Small sand isle is cut out of the shore. You see something there.",
                TextAfterExploration = "You almost manage to cross the river on feet, but strong current catches you and drags down the river. Finally you catch some old tree branches and pull yourself to the shore.",
                StatValues = new []
                {
                    new AddStatValue(HeroStats.Sticks, () => Random.Range(1, 3))
                },
                TileEnemy = Enemy.Crocodile,
                AttackChance = 30,
                Type = TileType.DrinkWater
            },
            new TileExplorationInfo
            {
                TextBeforeExploration = "You've found a watering hole. It's time to hunt.",
                TextAfterExploration = "You mark your target and make a leap. Hustle and bustle scares off the rest of the animals, but your prey is dead.",
                StatValues = new []
                {
                    new AddStatValue(HeroStats.Stamina, () => 2),
                    new AddStatValue(HeroStats.Hunger, () => 7),
                    new AddStatValue(HeroStats.Sticks, () => -2)
                },
                TileEnemy = null,
                AttackChance = 0,
                Type = TileType.DrinkWater
            },
            new TileExplorationInfo
            {
                TextBeforeExploration = "You've found a watering hole. It's time to hunt.",
                TextAfterExploration = "You try to sneak onto your prey but tree branch cracks under your feet. Nonetheless, luck is on your side and you manage to get a small game.",
                StatValues = new []
                {
                    new AddStatValue(HeroStats.Stamina, () => 1),
                    new AddStatValue(HeroStats.Hunger, () => Random.Range(4, 6))
                },
                TileEnemy = null,
                AttackChance = 0,
                Type = TileType.DrinkWater
            },
            new TileExplorationInfo
            {
                TextBeforeExploration = "You've found a watering hole. It's time to hunt.",
                TextAfterExploration = "You are the prey! Red-eyed hairy monster looks in your direction and you start to run.",
                StatValues = new []
                {
                    new AddStatValue(HeroStats.Stamina, () => -2),
                    new AddStatValue(HeroStats.Hunger, () => -2),
                    new AddStatValue(HeroStats.Vines, () => -2),
                    new AddStatValue(HeroStats.Sticks, () => -2),
                    new AddStatValue(HeroStats.Leaves, () => -2),
                    new AddStatValue(HeroStats.Wood, () => -1)
                },
                TileEnemy = null,
                AttackChance = 0,
                Type = TileType.DrinkWater
            },
            new TileExplorationInfo
            {
                TextBeforeExploration = "You approach a clearing. There are some familiar berries.",
                TextAfterExploration = "After sampling a couple you suddenly remember that they are not edible.",
                StatValues = new []
                {
                    new AddStatValue(HeroStats.Health, () => -1)
                },
                TileEnemy = Enemy.Snakes,
                AttackChance = 20,
                Type = TileType.Forest
            },
            new TileExplorationInfo
            {
                TextBeforeExploration = "You approach a clearing. There are some familiar berries.",
                TextAfterExploration = "There are enough berries to feast upon.",
                StatValues = new []
                {
                    new AddStatValue(HeroStats.Hunger, () => Random.Range(4, 6))
                },
                TileEnemy = Enemy.Beast,
                AttackChance = 20,
                Type = TileType.Forest
            },
            new TileExplorationInfo
            {
                TextBeforeExploration = "There is big fruit tree, standing amongst the thick brush.",
                TextAfterExploration = "You climb on the tree in hope to gain sweet fruits. Several attempts result in the strained hand.",
                StatValues = new AddStatValue[0],
                TileEnemy = Enemy.Beast,
                AttackChance = 70,
                Type = TileType.Jungle
            },
            new TileExplorationInfo
            {
                TextBeforeExploration = "There is big fruit tree, standing amongst the thick brush.",
                TextAfterExploration = "You climb on the tree in hope to gain sweet fruits. Several unsuccessfull attempts and finally you find yourself with just reward!",
                StatValues = new []
                {
                    new AddStatValue(HeroStats.Hunger, () => Random.Range(4, 6)),
                    new AddStatValue(HeroStats.Leaves, () => Random.Range(2, 3))
                },
                TileEnemy = Enemy.Beast,
                AttackChance = 30,
                Type = TileType.Jungle
            },
            new TileExplorationInfo
            {
                TextBeforeExploration = "There is big fruit tree, standing amongst the thick brush.",
                TextAfterExploration = "You climb on the tree in hope to gain sweet fruits. Several attempts result in the strained hand.",
                StatValues = new AddStatValue[0],
                TileEnemy = Enemy.Snakes,
                AttackChance = 70,
                Type = TileType.Jungle
            },
            new TileExplorationInfo
            {
                TextBeforeExploration = "Mosquitoes, swampy soil. It is not easy to traverse here.",
                TextAfterExploration = "Constant buzz is following you, they get in the ears and eyes. You try to gather at least a bit of materials",
                StatValues = new[]
                {
                    new AddStatValue(HeroStats.Leaves, () => Random.Range(2, 4)),
                    new AddStatValue(HeroStats.Vines, () => Random.Range(2, 4))
                },
                TileEnemy = Enemy.Bugs,
                AttackChance = 100,
                Type = TileType.Jungle
            },
            new TileExplorationInfo
            {
                TextBeforeExploration = "Mosquitoes, swampy soil. It is not easy to traverse here.",
                TextAfterExploration = "Constant buzz is following you, they get in the ears and eyes. You try to gather at least a bit of materials",
                StatValues = new[]
                {
                    new AddStatValue(HeroStats.Hunger, () => Random.Range(4, 6))
                },
                TileEnemy = Enemy.Bugs,
                AttackChance = 100,
                Type = TileType.Jungle
            },
            new TileExplorationInfo
            {
                TextBeforeExploration = "Impassable jungle.",
                TextAfterExploration = "Why are you even here..",
                StatValues = new AddStatValue[0],
                TileEnemy = Enemy.Snakes,
                AttackChance = 50,
                Type = TileType.Jungle
            },
            new TileExplorationInfo
            {
                TextBeforeExploration = "You walk in the jungle, breathing all the nature in. Bright acid colors and unusual plants are all around.",
                TextAfterExploration = "Vines are hanging around and you use this opportunity to refill tour stock.",
                StatValues = new[]
                {
                    new AddStatValue(HeroStats.Vines, 5)
                },
                TileEnemy = Enemy.Snakes,
                AttackChance = 20,
                Type = TileType.Jungle
            },
            new TileExplorationInfo
            {
                TextBeforeExploration = "You walk in the jungle, breathing all the nature in. Bright acid colors and unusual plants are all around.",
                TextAfterExploration = "Herbs! You craft bandage from the leaves. You may not need it now, but who knows..",
                StatValues = new[]
                {
                    new AddStatValue(HeroStats.Health, 4)
                },
                TileEnemy = Enemy.Beast,
                AttackChance = 20,
                Type = TileType.Jungle
            },
            new TileExplorationInfo
            {
                TextBeforeExploration = "There are bunches of exotic plants around, maybe some of them are even edible.",
                TextAfterExploration = "You should be careful with such things. But you was not. You feel sick.",
                StatValues = new[]
                {
                    new AddStatValue(HeroStats.Health, -1)
                },
                TileEnemy = Enemy.Snakes,
                AttackChance = 20,
                Type = TileType.Jungle
            },
            new TileExplorationInfo
            {
                TextBeforeExploration = "There are bunches of exotic plants around, maybe some of them are even edible.",
                TextAfterExploration = "You should be careful with such things. You gather only those fruits, that are known to you.",
                StatValues = new[]
                {
                    new AddStatValue(HeroStats.Hunger, () => Random.Range(3, 5))
                },
                TileEnemy = Enemy.Bugs,
                AttackChance = 20,
                Type = TileType.Jungle
            },
            new TileExplorationInfo
            {
                TextBeforeExploration = "Something is grazing nearby. You can try and track it.",
                TextAfterExploration = "Small game, berries, game again. It is definitely one lucky day",
                StatValues = new []
                {
                    new AddStatValue(HeroStats.Hunger, () => Random.Range(4, 6))
                },
                TileEnemy = Enemy.Bear,
                AttackChance = 50,
                Type = TileType.Forest
            },
            new TileExplorationInfo
            {
                TextBeforeExploration = "Something is grazing nearby. You can try and track it.",
                TextAfterExploration = "You fail to catch anything, but there are enough berries to eat.",
                StatValues = new []
                {
                    new AddStatValue(HeroStats.Hunger, () => Random.Range(2, 4))
                },
                TileEnemy = Enemy.Deer,
                AttackChance = 30,
                Type = TileType.Forest
            },
            new TileExplorationInfo
            {
                TextBeforeExploration = "Something is grazing nearby. You can try and track it.",
                TextAfterExploration = "You set up primitive traps and check your old ones.",
                StatValues = new []
                {
                    new AddStatValue(HeroStats.Hunger, () => Random.Range(1, 6)),
                    new AddStatValue(HeroStats.Vines, () => -2)
                },
                TileEnemy = null,
                AttackChance = 0,
                Type = TileType.Forest
            },
            new TileExplorationInfo
            {
                TextBeforeExploration = "Smoke is rising not so far away.",
                TextAfterExploration = "Stumbling through the forest you try to track the source of the smoke.",
                StatValues = new []
                {
                    new AddStatValue(HeroStats.Sticks, () => Random.Range(3, 4)),
                    new AddStatValue(HeroStats.Leaves, () => 1)
                },
                TileEnemy = Enemy.Flock,
                AttackChance = 50,
                Type = TileType.Forest
            },
            new TileExplorationInfo
            {
                TextBeforeExploration = "Forest becomes thicker and it is really hard to traverse. But it is exactly where you can get more of that precious logs.",
                TextAfterExploration = "Branches tear your clothes apart and you are starting to feel sorry about venturing this deep. But at least you've found what you wanted!",
                StatValues = new []
                {
                    new AddStatValue(HeroStats.Wood, () => Random.Range(2, 3)),
                    new AddStatValue(HeroStats.Leaves, () => Random.Range(1, 2)),
                    new AddStatValue(HeroStats.Sticks, () => Random.Range(1, 4))
                },
                TileEnemy = Enemy.Bugs,
                AttackChance = 100,
                Type = TileType.Forest
            },
            new TileExplorationInfo
            {
                TextBeforeExploration = "Forest becomes thicker and it is really hard to traverse. But it is exactly where you can get more of that precious logs.",
                TextAfterExploration = "It is hard to see but you've found someone's tracks nonetheless.",
                StatValues = new AddStatValue[0],
                TileEnemy = Enemy.Bear,
                AttackChance = 100,
                Type = TileType.Forest
            },
            new TileExplorationInfo
            {
                TextBeforeExploration = "You feel somebodies eyes on your back. This feeling makes you nervous.",
                TextAfterExploration = "Hunt becomes hide-and-seek, but you can't be cautious enough. There is a hatch under the handful of leaves. You knock, then you try to pry it open, but all in vain. You set up a small camp, hoping that someone will come back.",
                StatValues = new []
                {
                    new AddStatValue(HeroStats.Hunger, () => -1),
                    new AddStatValue(HeroStats.Health, () => 3)
                },
                Type = TileType.Forest
            },
            //stubs
            new TileExplorationInfo
            {
                TextBeforeExploration = "Empty beach, lifeless shore is not really inspiring.",
                TextAfterExploration = "You walk along the shore, trying to find anything usefull.",
                StatValues = new []
                {
                    new AddStatValue(HeroStats.Rocks, () => Random.Range(0, 1)),
                    new AddStatValue(HeroStats.Sticks, () => Random.Range(0, 1))
                },
                TileEnemy = Enemy.Beast,
                AttackChance = 10,
                Type = TileType.Sand
            },
            new TileExplorationInfo
            {
                TextBeforeExploration = "Empty beach, lifeless shore is not really inspiring.",
                TextAfterExploration = "You walk along the shore, trying to find anything usefull.",
                StatValues = new []
                {
                    new AddStatValue(HeroStats.Rocks, () => Random.Range(0, 1)),
                    new AddStatValue(HeroStats.Sticks, () => Random.Range(0, 1))
                },
                TileEnemy = Enemy.Beast,
                AttackChance = 10,
                Type = TileType.Sand
            },
            new TileExplorationInfo
            {
                TextBeforeExploration = "Empty beach, lifeless shore is not really inspiring.",
                TextAfterExploration = "You walk along the shore, trying to find anything usefull.",
                StatValues = new []
                {
                    new AddStatValue(HeroStats.Rocks, () => Random.Range(0, 1)),
                    new AddStatValue(HeroStats.Sticks, () => Random.Range(0, 1))
                },
                TileEnemy = Enemy.Beast,
                AttackChance = 10,
                Type = TileType.Sand
            },
            new TileExplorationInfo
            {
                TextBeforeExploration = "Empty beach, lifeless shore is not really inspiring.",
                TextAfterExploration = "You walk along the shore, trying to find anything usefull.",
                StatValues = new []
                {
                    new AddStatValue(HeroStats.Rocks, () => Random.Range(0, 1)),
                    new AddStatValue(HeroStats.Sticks, () => Random.Range(0, 1))
                },
                TileEnemy = Enemy.Beast,
                AttackChance = 10,
                Type = TileType.Sand
            },
            new TileExplorationInfo
            {
                TextBeforeExploration = "Empty beach, lifeless shore is not really inspiring.",
                TextAfterExploration = "You walk along the shore, trying to find anything usefull.",
                StatValues = new []
                {
                    new AddStatValue(HeroStats.Rocks, () => Random.Range(0, 1)),
                    new AddStatValue(HeroStats.Sticks, () => Random.Range(0, 1))
                },
                TileEnemy = Enemy.Beast,
                AttackChance = 10,
                Type = TileType.Sand
            },
            new TileExplorationInfo
            {
                TextBeforeExploration = "Empty beach, lifeless shore is not really inspiring.",
                TextAfterExploration = "You walk along the shore, trying to find anything usefull.",
                StatValues = new []
                {
                    new AddStatValue(HeroStats.Rocks, () => Random.Range(0, 1)),
                    new AddStatValue(HeroStats.Sticks, () => Random.Range(0, 1))
                },
                TileEnemy = Enemy.Beast,
                AttackChance = 10,
                Type = TileType.Sand
            },
            new TileExplorationInfo
            {
                TextBeforeExploration = "Quiet place with plenty of water to drink.",
                TextAfterExploration = "After drinking sweet clean water you continue your way.",
                StatValues = new []
                {
                    new AddStatValue(HeroStats.Stamina, () => Random.Range(4, 5))
                },
                TileEnemy = Enemy.Beast,
                AttackChance = 10,
                Type = TileType.DrinkWater
            },
            new TileExplorationInfo
            {
                TextBeforeExploration = "Quiet place with plenty of water to drink.",
                TextAfterExploration = "After drinking sweet clean water you continue your way.",
                StatValues = new []
                {
                    new AddStatValue(HeroStats.Stamina, () => Random.Range(4, 5))
                },
                TileEnemy = Enemy.Beast,
                AttackChance = 10,
                Type = TileType.DrinkWater
            },
            new TileExplorationInfo
            {
                TextBeforeExploration = "Quiet place with plenty of water to drink.",
                TextAfterExploration = "After drinking sweet clean water you continue your way.",
                StatValues = new []
                {
                    new AddStatValue(HeroStats.Stamina, () => Random.Range(4, 5))
                },
                TileEnemy = Enemy.Beast,
                AttackChance = 10,
                Type = TileType.DrinkWater
            },
            new TileExplorationInfo
            {
                TextBeforeExploration = "Quiet place with plenty of water to drink.",
                TextAfterExploration = "After drinking sweet clean water you continue your way.",
                StatValues = new []
                {
                    new AddStatValue(HeroStats.Stamina, () => Random.Range(4, 5))
                },
                TileEnemy = Enemy.Beast,
                AttackChance = 10,
                Type = TileType.DrinkWater
            },
            new TileExplorationInfo
            {
                TextBeforeExploration = "Quiet place with plenty of water to drink.",
                TextAfterExploration = "After drinking sweet clean water you continue your way.",
                StatValues = new []
                {
                    new AddStatValue(HeroStats.Stamina, () => Random.Range(4, 5))
                },
                TileEnemy = Enemy.Beast,
                AttackChance = 10,
                Type = TileType.DrinkWater
            },
            new TileExplorationInfo
            {
                TextBeforeExploration = "Quiet place with plenty of water to drink.",
                TextAfterExploration = "After drinking sweet clean water you continue your way.",
                StatValues = new []
                {
                    new AddStatValue(HeroStats.Stamina, () => Random.Range(4, 5))
                },
                TileEnemy = Enemy.Beast,
                AttackChance = 10,
                Type = TileType.DrinkWater
            },
            new TileExplorationInfo
            {
                TextBeforeExploration = "Quiet place with plenty of water to drink.",
                TextAfterExploration = "After drinking sweet clean water you continue your way.",
                StatValues = new []
                {
                    new AddStatValue(HeroStats.Stamina, () => Random.Range(4, 5))
                },
                TileEnemy = Enemy.Beast,
                AttackChance = 10,
                Type = TileType.DrinkWater
            },
            new TileExplorationInfo
            {
                TextBeforeExploration = "Brush is almost impassable.",
                TextAfterExploration = "You look around and choose vines that look more or less solid.",
                StatValues = new []
                {
                    new AddStatValue(HeroStats.Vines, () => Random.Range(2, 4)),
                    new AddStatValue(HeroStats.Leaves, () => Random.Range(2, 3)),
                    new AddStatValue(HeroStats.Hunger, () => Random.Range(2, 4))
                },
                TileEnemy = Enemy.Beast,
                AttackChance = 10,
                Type = TileType.Jungle
            },
            new TileExplorationInfo
            {
                TextBeforeExploration = "Brush is almost impassable.",
                TextAfterExploration = "You look around and choose vines that look more or less solid.",
                StatValues = new []
                {
                    new AddStatValue(HeroStats.Vines, () => Random.Range(2, 4)),
                    new AddStatValue(HeroStats.Leaves, () => Random.Range(2, 3)),
                    new AddStatValue(HeroStats.Hunger, () => Random.Range(2, 4))
                },
                TileEnemy = Enemy.Beast,
                AttackChance = 10,
                Type = TileType.Jungle
            },
            new TileExplorationInfo
            {
                TextBeforeExploration = "Brush is almost impassable.",
                TextAfterExploration = "You look around and choose vines that look more or less solid.",
                StatValues = new []
                {
                    new AddStatValue(HeroStats.Vines, () => Random.Range(2, 4)),
                    new AddStatValue(HeroStats.Leaves, () => Random.Range(2, 3)),
                    new AddStatValue(HeroStats.Hunger, () => Random.Range(2, 4))
                },
                TileEnemy = Enemy.Beast,
                AttackChance = 10,
                Type = TileType.Jungle
            },
            new TileExplorationInfo
            {
                TextBeforeExploration = "Brush is almost impassable.",
                TextAfterExploration = "You look around and choose vines that look more or less solid.",
                StatValues = new []
                {
                    new AddStatValue(HeroStats.Vines, () => Random.Range(2, 4)),
                    new AddStatValue(HeroStats.Leaves, () => Random.Range(2, 3)),
                    new AddStatValue(HeroStats.Hunger, () => Random.Range(2, 4))
                },
                TileEnemy = Enemy.Beast,
                AttackChance = 10,
                Type = TileType.Jungle
            },
            new TileExplorationInfo
            {
                TextBeforeExploration = "Brush is almost impassable.",
                TextAfterExploration = "You look around and choose vines that look more or less solid.",
                StatValues = new []
                {
                    new AddStatValue(HeroStats.Vines, () => Random.Range(2, 4)),
                    new AddStatValue(HeroStats.Leaves, () => Random.Range(2, 3)),
                    new AddStatValue(HeroStats.Hunger, () => Random.Range(2, 4))
                },
                TileEnemy = Enemy.Beast,
                AttackChance = 10,
                Type = TileType.Jungle
            },
            new TileExplorationInfo
            {
                TextBeforeExploration = "Brush is almost impassable.",
                TextAfterExploration = "You look around and choose vines that look more or less solid.",
                StatValues = new []
                {
                    new AddStatValue(HeroStats.Vines, () => Random.Range(2, 4)),
                    new AddStatValue(HeroStats.Leaves, () => Random.Range(2, 3)),
                    new AddStatValue(HeroStats.Hunger, () => Random.Range(2, 4))
                },
                TileEnemy = Enemy.Beast,
                AttackChance = 10,
                Type = TileType.Jungle
            },
            new TileExplorationInfo
            {
                TextBeforeExploration = "Brush is almost impassable.",
                TextAfterExploration = "You look around and choose vines that look more or less solid.",
                StatValues = new []
                {
                    new AddStatValue(HeroStats.Vines, () => Random.Range(2, 4)),
                    new AddStatValue(HeroStats.Leaves, () => Random.Range(2, 3)),
                    new AddStatValue(HeroStats.Hunger, () => Random.Range(2, 4))
                },
                TileEnemy = Enemy.Beast,
                AttackChance = 10,
                Type = TileType.Jungle
            },
            new TileExplorationInfo
            {
                TextBeforeExploration = "You are going deeper and deeper into the forest, way out becomes blurry in your mind.",
                TextAfterExploration = "After some time your feel yourself lost. Were you here already?",
                StatValues = new []
                {
                    new AddStatValue(HeroStats.Vines, () => Random.Range(2, 3)),
                    new AddStatValue(HeroStats.Leaves, () => Random.Range(0, 2)),
                    new AddStatValue(HeroStats.Hunger, () => Random.Range(1, 3)),
                    new AddStatValue(HeroStats.Wood, () => 1)
                },
                TileEnemy = Enemy.Beast,
                AttackChance = 10,
                Type = TileType.Forest
            },
            new TileExplorationInfo
            {
                TextBeforeExploration = "You are going deeper and deeper into the forest, way out becomes blurry in your mind.",
                TextAfterExploration = "After some time your feel yourself lost. Were you here already?",
                StatValues = new []
                {
                    new AddStatValue(HeroStats.Vines, () => Random.Range(2, 3)),
                    new AddStatValue(HeroStats.Leaves, () => Random.Range(0, 2)),
                    new AddStatValue(HeroStats.Hunger, () => Random.Range(1, 3)),
                    new AddStatValue(HeroStats.Wood, () => 1)
                },
                TileEnemy = Enemy.Beast,
                AttackChance = 10,
                Type = TileType.Forest
            },
            new TileExplorationInfo
            {
                TextBeforeExploration = "You are going deeper and deeper into the forest, way out becomes blurry in your mind.",
                TextAfterExploration = "After some time your feel yourself lost. Were you here already?",
                StatValues = new []
                {
                    new AddStatValue(HeroStats.Vines, () => Random.Range(2, 3)),
                    new AddStatValue(HeroStats.Leaves, () => Random.Range(0, 2)),
                    new AddStatValue(HeroStats.Hunger, () => Random.Range(1, 3)),
                    new AddStatValue(HeroStats.Wood, () => 1)
                },
                TileEnemy = Enemy.Beast,
                AttackChance = 10,
                Type = TileType.Forest
            },
            new TileExplorationInfo
            {
                TextBeforeExploration = "You are going deeper and deeper into the forest, way out becomes blurry in your mind.",
                TextAfterExploration = "After some time your feel yourself lost. Were you here already?",
                StatValues = new []
                {
                    new AddStatValue(HeroStats.Vines, () => Random.Range(2, 3)),
                    new AddStatValue(HeroStats.Leaves, () => Random.Range(0, 2)),
                    new AddStatValue(HeroStats.Hunger, () => Random.Range(1, 3)),
                    new AddStatValue(HeroStats.Wood, () => 1)
                },
                TileEnemy = Enemy.Beast,
                AttackChance = 10,
                Type = TileType.Forest
            },
            new TileExplorationInfo
            {
                TextBeforeExploration = "You are going deeper and deeper into the forest, way out becomes blurry in your mind.",
                TextAfterExploration = "After some time your feel yourself lost. Were you here already?",
                StatValues = new []
                {
                    new AddStatValue(HeroStats.Vines, () => Random.Range(2, 3)),
                    new AddStatValue(HeroStats.Leaves, () => Random.Range(0, 2)),
                    new AddStatValue(HeroStats.Hunger, () => Random.Range(1, 3)),
                    new AddStatValue(HeroStats.Wood, () => 1)
                },
                TileEnemy = Enemy.Beast,
                AttackChance = 10,
                Type = TileType.Forest
            },
            new TileExplorationInfo
            {
                TextBeforeExploration = "You are going deeper and deeper into the forest, way out becomes blurry in your mind.",
                TextAfterExploration = "After some time your feel yourself lost. Were you here already?",
                StatValues = new []
                {
                    new AddStatValue(HeroStats.Vines, () => Random.Range(2, 3)),
                    new AddStatValue(HeroStats.Leaves, () => Random.Range(0, 2)),
                    new AddStatValue(HeroStats.Hunger, () => Random.Range(1, 3)),
                    new AddStatValue(HeroStats.Wood, () => 1)
                },
                TileEnemy = Enemy.Beast,
                AttackChance = 10,
                Type = TileType.Forest
            },
            new TileExplorationInfo
            {
                TextBeforeExploration = "You are going deeper and deeper into the forest, way out becomes blurry in your mind.",
                TextAfterExploration = "After some time your feel yourself lost. Were you here already?",
                StatValues = new []
                {
                    new AddStatValue(HeroStats.Vines, () => Random.Range(2, 3)),
                    new AddStatValue(HeroStats.Leaves, () => Random.Range(0, 2)),
                    new AddStatValue(HeroStats.Hunger, () => Random.Range(1, 3)),
                    new AddStatValue(HeroStats.Wood, () => 1)
                },
                TileEnemy = Enemy.Beast,
                AttackChance = 10,
                Type = TileType.Forest
            },
        };

        #endregion
        
        //ScreenCapture.CaptureScreenshot(Application.dataPath + "/screenshots/" + DateTime.Now.ToString("yyyy-MM-dd HH-mm-ss") + ".png");
        //UnityEditor.AssetDatabase.Refresh();
        _eventChance = eventChanceStart;
        _gridInformation = gridData.GetComponent<GridInformation>();
        var tiles = gridData.GetComponentsInChildren<BaseTile>();
        var waterInfos = _explorationInfos.Where(ei => ei.Type == TileType.DrinkWater).ToArray();
        foreach (var tile in tiles)
        {
            var coords = gridData.WorldToCell(tile.transform.position);
            _gridInformation.SetPositionProperty(coords, $"tile {coords}", (UnityEngine.Object)tile);

            if (tile.isStart)
            {
                tile.SetExplorationInfo(TileExplorationInfo.Camp);
                tile.SetResearched(true);
                SetCurrentTile(tile);
            }

            if (tile is SandTile sandTile)
            {
                if (sandTile.isAlternateEnding)
                {
                    alternateEndingTile = sandTile;
                }
            }

            if (tile.type == TileType.DrinkWater)
            {
                var index = Random.Range(0, waterInfos.Length);
                tile.SetExplorationInfo(waterInfos[index]);
            }
        }
        
        var currentCoords = gridData.WorldToCell(currentTile.transform.position);
        UpdateCanOpenTiles(currentCoords);
    }

    public void UpdateCanOpenTiles(Vector3Int coord)
    {
        var minPosition = new Vector3Int(coord.x - 1, coord.y - 1, coord.z);

        for (int i = 0; i < 3; i++)
            for (int j = 0; j < 3; j++)
            {
                var tileCoord = new Vector3Int(minPosition.x + i, minPosition.y + j, coord.z);
                if (coord.Equals(tileCoord)) continue;
                    
                var tile = _gridInformation.GetPositionProperty<BaseTile>(tileCoord, $"tile {tileCoord}", null);
                if (tile != null && !tile.opened) tile.SetCanOpen(true);
            }
    }

    public bool CanOpen(Vector3Int coord)
    {
        var minPosition = new Vector3Int(coord.x - 1, coord.y - 1, coord.z);

        for (int i = 0; i < 3; i++)
            for (int j = 0; j < 3; j++)
            {
                var tileCoord = new Vector3Int(minPosition.x + i, minPosition.y + j, coord.z);
                if (coord.Equals(tileCoord)) continue;
                
                var tile = _gridInformation.GetPositionProperty<BaseTile>(tileCoord, $"tile {tileCoord}", null);
                if (tile != null && tile.opened && !(tile is SeaTile)) return true;
            }
        
        return false;
    }

    public void SetCurrentTile(BaseTile newTile)
    {
        currentTile = newTile;
        
        uiController.UpdateTileInfo(currentTile);
    }

    public TileEvent GetEvent(TileType type)
    {
        var chance = Random.value * 100;
        var chance2 = Random.value * 100;

        TileEvent @event = null;
        if (chance < _eventChance && chance2 < _eventChance)
        {
            var probableEvents = _events.Where(e => !e.Executed && e.Types.Contains(type)).ToArray();
            if (probableEvents.Length != 0)
            {
                var index = Random.Range (0, probableEvents.Length);
                @event = probableEvents[index];
                @event.Executed = true;
                _eventChance = eventChanceStart;
            }
        }
        else
        {
            _eventChance += eventChanceIncrement;
        }

        return @event;
    }

    public TileExplorationInfo GetExplorationInfo(TileType type)
    {
        var chance = Random.value * 100;
        var chance2 = Random.value * 100;
        var haveExploration = chance <= explorationChance && chance2 <= explorationChance;
        if (!haveExploration) return null;

        var explorations = _explorationInfos.Where(ei => ei.Type == type).ToArray();
        if (explorations.Length == 0) return null;

        var index = Random.Range(0, explorations.Length);
        return explorations[index];
    }

    public void ActivateAlternateEnding()
    {
        alternateEndingTile.ActivateAlternateEnding();
    }
}
