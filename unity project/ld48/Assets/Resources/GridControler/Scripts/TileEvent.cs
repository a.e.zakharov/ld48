public class TileEvent
{
    public string Name { get; set; }
    
    public string Text { get; set; }
    
    public AddStatValue[] StatsChange { get; set; }
    
    public TileType[] Types { get; set; }
    
    public bool Executed { get; set; }
}
