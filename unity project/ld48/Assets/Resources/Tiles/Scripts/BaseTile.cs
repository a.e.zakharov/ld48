using System;
using System.Linq;
using System.Text;
using Resources.GridControler.Scripts;
using UnityEngine;

public class BaseTile : MonoBehaviour, ITile
{
    public bool opened;

    public bool isStart;

	public AudioClip audioClip;
    
    public Transform coverTile;

    public Transform exploredTile;

    public Transform shadowTile;

    public TileType type;
    
    public TileExplorationInfo tileExplorationInfo { get; protected set; }
    
    public bool researched { get; protected set; }

    protected bool _canOpen;

    protected Grid _grid;

    protected GridController _gridController;

    protected UiController _uiController;

	protected AudioController _audioController;

    protected HeroStats _heroStats;
    
    private void Awake()
    {
        if (isStart)
        {
            opened = isStart;
        }

        _grid = FindObjectOfType<Grid>();
        _gridController = FindObjectOfType<GridController>();
        _uiController = FindObjectOfType<UiController>();
		_audioController = FindObjectOfType<AudioController>();
        _heroStats = FindObjectOfType<HeroStats>();
    }

    private void Update()
    {
        coverTile.gameObject.SetActive(_canOpen);
        shadowTile.gameObject.SetActive(!opened && !_canOpen);
        exploredTile.gameObject.SetActive(!researched && !string.IsNullOrEmpty(tileExplorationInfo?.TextAfterExploration));
    }

    public void SetCanOpen(bool value)
    {
        _canOpen = value;
    }

    public void SetResearched(bool value)
    {
        researched = value;
        if (value && tileExplorationInfo != TileExplorationInfo.Camp)
        {
            tileExplorationInfo = TileExplorationInfo.Default;
        }
    }

    private void OnMouseUpAsButton()
    {
		_audioController.playClip(audioClip);
        var coordinate = _grid.WorldToCell(transform.position);
        if (opened)
        {
            _gridController.SetCurrentTile(this);
            return;
        }
        
        if (!_uiController.canOpenTiles || !_gridController.CanOpen(coordinate))
        {
            return;
        }

        opened = true;
        SetCanOpen(false);

        if (tileExplorationInfo == null) tileExplorationInfo = _gridController.GetExplorationInfo(type);
        if (tileExplorationInfo == null)
        {
            SetResearched(true);
        }

        _gridController.SetCurrentTile(this);
        _gridController.UpdateCanOpenTiles(coordinate);

        GenerateEvent();
    }

    public void SetExplorationInfo(TileExplorationInfo explorationInfo)
    {
        tileExplorationInfo = explorationInfo;
    }

    public virtual void GenerateEvent()
    {
        _heroStats.AddStats(new []{new AddStatValue(HeroStats.Hunger, -1)});
        var tileEvent = _gridController.GetEvent(type);
        if (tileEvent == null) return;
        
        _heroStats.AddStats(tileEvent.StatsChange);

        var sb = new StringBuilder();

        sb.Append($"<size=+5>{tileEvent.Name}</size>\n\n{tileEvent.Text}");
        if (tileEvent.StatsChange.Any())
        {
            sb.Append("\n\n" + String.Join("\n", tileEvent.StatsChange.Where(sc => sc.AddValue != 0).Select(se =>
            {
                var addValueStr = se.AddValue > 0 ? $"+{se.AddValue}" : se.AddValue.ToString();
                return $"<b>{se.StatName}</b>: {addValueStr}";
            })));
        }
        
        _uiController.ShowText(sb.ToString());
    }
}
