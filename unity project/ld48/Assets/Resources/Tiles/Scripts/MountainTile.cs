using System;
using System.Linq;
using System.Text;
using Random = UnityEngine.Random;

public class MountainTile : BaseTile
{
    public bool isViewPoint;

    public int viewPointChance;

    public override void GenerateEvent()
    {
        if (!isViewPoint)
        {
            base.GenerateEvent();
            return;
        }

        var chance = Random.value * 100;
        var chance2 = Random.value * 100;
        var isChance = chance <= viewPointChance && chance2 <= viewPointChance;
        if (!isChance) return;
        
        _heroStats.AddStats(new []{new AddStatValue(HeroStats.Hunger, -1)});
        var tileEvent = new TileEvent
        {
            Name = "Viewpoint",
            Text = "You can see the bay from here. And lo and behold, there is something floating in there, maybe it could be your chance to leave this place!",
            StatsChange = Array.Empty<AddStatValue>(),
        };
        
        _heroStats.AddStats(tileEvent.StatsChange);

        var sb = new StringBuilder();

        sb.Append($"<size=+5>{tileEvent.Name}</size>\n\n{tileEvent.Text}");
        _uiController.ShowText(sb.ToString());
        _gridController.ActivateAlternateEnding();
    }
}
