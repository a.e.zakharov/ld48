using Resources.GridControler.Scripts;
using UnityEngine;

public class SandTile : BaseTile
{
    public Transform alternateEndingTile;
    
    public bool isAlternateEnding;

    private bool _isAlternateEndingActivated;
    
    private void Awake()
    {
        if (isStart)
        {
            opened = isStart;
        }

        _grid = FindObjectOfType<Grid>();
        _gridController = FindObjectOfType<GridController>();
        _uiController = FindObjectOfType<UiController>();
        _audioController = FindObjectOfType<AudioController>();
        _heroStats = FindObjectOfType<HeroStats>();
        
        alternateEndingTile.gameObject.SetActive(false);
    }

    private void Update()
    {
        if(_isAlternateEndingActivated) return;
        coverTile.gameObject.SetActive(_canOpen);
        shadowTile.gameObject.SetActive(!opened && !_canOpen);
        exploredTile.gameObject.SetActive(!researched);
    }
    
    public void ActivateAlternateEnding()
    {
        _isAlternateEndingActivated = true;
        alternateEndingTile.gameObject.SetActive(true);
        coverTile.gameObject.SetActive(false);
        exploredTile.gameObject.SetActive(false);
        shadowTile.gameObject.SetActive(false);
        SetResearched(false);
        SetExplorationInfo(new TileExplorationInfo
        {
            TextBeforeExploration = "Leave island",
            StatValues = new []
            {
                new AddStatValue(HeroStats.AlternateEnding, 1),
            }
        });
    }
}
