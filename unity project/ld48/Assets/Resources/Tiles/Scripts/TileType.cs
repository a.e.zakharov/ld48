public enum TileType 
{
    None,
    
    Forest,
    
    DrinkWater,
    
    Jungle,
    
    Mountain,
    
    Sand,
    
    Sea
}
